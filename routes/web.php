<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', function(){
    return view('admin');
});
/* #region Country */ 
Route::resource('/admin/country','CountryController');

Route::get('/admin/country/create',['uses' => 'CountryController@create']);

Route::get('/admin/country/read/{id}',['uses' => 'CountryController@show']);

Route::get('/admin/country/edit/{id}',['uses' => 'CountryController@edit']);

Route::get('/admin/country/delete/{id}',['uses' => 'CountryController@destroy']);

Route::post('/admin/country/store',['uses' => 'CountryController@store']);

Route::post('/admin/country/update',['uses' => 'CountryController@update']);

Route::post('/admin/country/delete-confirmed',['uses' => 'CountryController@delete']);

/* #endregion */

/* #region Role */

Route::resource('/admin/role','RoleController');

Route::get('/admin/role/create',['uses' => 'RoleController@create']);

Route::get('/admin/role/read/{id}',['uses' => 'RoleController@show']);

Route::get('/admin/role/edit/{id}',['uses' => 'RoleController@edit']);

Route::get('/admin/role/delete/{id}',['uses' => 'RoleController@destroy']);

Route::post('/admin/role/store',['uses' => 'RoleController@store']);

Route::post('/admin/role/update',['uses' => 'RoleController@update']);

Route::post('/admin/role/delete-confirmed',['uses' => 'RoleController@delete']);

/* #endregion */

/* EventCategory */

Route::resource('/admin/eventcategory','EventCategoryController');

Route::get('/admin/eventcategory/create',['uses' => 'EventCategoryController@create']);

Route::get('/admin/eventcategory/read/{id}',['uses' => 'EventCategoryController@show']);

Route::get('/admin/eventcategory/edit/{id}',['uses' => 'EventCategoryController@edit']);

Route::get('/admin/eventcategory/delete/{id}',['uses' => 'EventCategoryController@destroy']);

Route::post('/admin/eventcategory/store',['uses' => 'EventCategoryController@store']);

Route::post('/admin/eventcategory/update',['uses' => 'EventCategoryController@update']);

Route::post('/admin/eventcategory/delete-confirmed',['uses' => 'EventCategoryController@delete']);

/**/

/* EventCategory */

Route::resource('/admin/eventtopic','EventTopicController');

Route::get('/admin/eventtopic/create',['uses' => 'EventTopicController@create']);

Route::get('/admin/eventtopic/read/{id}',['uses' => 'EventTopicController@show']);

Route::get('/admin/eventtopic/edit/{id}',['uses' => 'EventTopicController@edit']);

Route::get('/admin/eventtopic/delete/{id}',['uses' => 'EventTopicController@destroy']);

Route::post('/admin/eventtopic/store',['uses' => 'EventTopicController@store']);

Route::post('/admin/eventtopic/update',['uses' => 'EventTopicController@update']);

Route::post('/admin/eventtopic/delete-confirmed',['uses' => 'EventTopicController@delete']);

/**/

/* Event */

Route::resource('/admin/event','EventController');

Route::get('/admin/event/create',['uses' => 'EventController@create']);

Route::get('/admin/event/read/{id}',['uses' => 'EventController@show']);

Route::get('/admin/event/edit/{id}',['uses' => 'EventController@edit']);

Route::get('/admin/event/delete/{id}',['uses' => 'EventController@destroy']);

Route::post('/admin/event/store',['uses' => 'EventController@store']);

Route::post('/admin/event/update',['uses' => 'EventController@update']);

Route::post('/admin/event/delete-confirmed',['uses' => 'EventController@delete']);

/**/