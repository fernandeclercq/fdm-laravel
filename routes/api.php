<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* Country */ 
Route::post('/admin/country/store',['uses' => 'CountryController@store']);

Route::post('/admin/country/update',['uses' => 'CountryController@update']);

Route::post('/admin/country/delete-confirmed',['uses' => 'CountryController@delete']);
/**/

/* Role */

Route::post('/admin/role/store',['uses' => 'RoleController@store']);

Route::post('/admin/role/update',['uses' => 'RoleController@update']);

Route::post('/admin/role/delete-confirmed',['uses' => 'RoleController@delete']);

/**/

/* EventCategory */

Route::post('/admin/eventcategory/store',['uses' => 'EventCategoryController@store']);

Route::post('/admin/eventcategory/update',['uses' => 'EventCategoryController@update']);

Route::post('/admin/eventcategory/delete-confirmed',['uses' => 'EventCategoryController@delete']);

/**/

/* EventTopic */

Route::post('/admin/eventtopic/store',['uses' => 'EventTopicController@store']);

Route::post('/admin/eventtopic/update',['uses' => 'EventTopicController@update']);

Route::post('/admin/eventtopic/delete-confirmed',['uses' => 'EventTopicController@delete']);

/**/

/* Event */

Route::post('/admin/event/store',['uses' => 'EventController@store']);

Route::post('/admin/event/update',['uses' => 'EventController@update']);

Route::post('/admin/event/delete-confirmed',['uses' => 'EventController@delete']);

/**/