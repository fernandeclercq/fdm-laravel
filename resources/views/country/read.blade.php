@extends('layouts.app')
@section('content')
    <header>
        <div class="back-btn">
            <a href="{{secure_asset('admin')}}">Website<br>Index</a>
        </div>
        <h1>Fric-frac</h1>
    </header>
    <main>
    <div class="container-index-model">
    <div class="index-column-0">
        <div class="index-row-1">
            <div class="model-name"><p>Country</p></div>
            <div class="btn">
                <div><a href="{{secure_asset("/admin/country/edit/$country->Id")}}">Update</a></div>
                <div><a href="{{secure_asset('/admin/country/create')}}">Create</a></div>
                <div><a href="{{secure_asset("/admin/country/delete/$country->Id")}}">Delete</a></div>
                <div><a href="{{secure_asset('/admin/country')}}">Annuleren</a></div>
            </div>
        </div>
        <div class="index-row-2">
        <form action="" method="POST">
                @csrf
                <div class="details-info">
                    <h3>Details - {{$country->Name}}</h3>
                    <p>Select the next action please</p>
                </div>
                <div class="form-fields">
                    <label for="Name">Naam</label>
                <input type="text" name="Name" readonly value="{{$country->Name}}">
                </div>
                <div class="form-fields">        
                    <label for="Code">Code</label>
                <input type="text" name="Code" readonly value="{{$country->Code}}">
                </div>
                
            </form>
        </div>
    </div>
        
        <div class="index-column-1">
            @include('country.select',$countries)

        </div>        
    </div>
</main>
@endsection



