@extends('layouts.app')
@section('content')
    <header>
        <div class="back-btn">
            <a href="{{secure_asset('admin')}}">Website<br>Index</a>
        </div>
        <h1>Fric-frac</h1>
    </header>
    <main>
    <div class="container-index-model">
    <div class="index-column-0">
        <div class="index-row-1">
            <div class="model-name"><p>Country</p></div>
            <div class="btn">
                <div><p>Update</p></div>
                <div><a href="{{secure_asset('/admin/country')}}">Annuleren</a></div>
            </div>
        </div>
        <div class="index-row-2">
        <form action="{{secure_asset('admin/country/update')}}" method="POST">
                @csrf
                <div class="details-info">
                    <h3>Edit - {{$country->Name}}</h3>
                    <p>Introduce the new data and click on Update</p>
                </div>
                    <input type="hidden" name="Id" readonly value="{{$country->Id}}">
                <div class="form-fields">
                    <label for="Name">Naam</label>
                <input type="text" name="Name" value="{{$country->Name}}">
                </div>
                <div class="form-fields">        
                    <label for="Code">Code</label>
                <input type="text" name="Code" value="{{$country->Code}}">
                </div>
                <div class="submit-field">
                    <button type="submit" name="update">Update</button>
                </div>
                
            </form>
        </div>
    </div>
        
        <div class="index-column-1">
            @include('country.select',$countries)

        </div>        
    </div>
</main>
@endsection



