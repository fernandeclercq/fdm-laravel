@extends('layouts.app')
@section('content')
    <header>
        <div class="back-btn">
            <a href="{{secure_asset('admin')}}">Website<br>Index</a>
        </div>
        <h1>Fric-frac</h1>
    </header>
    <main>
    <div class="container-index-model">
            <div class="index-column-0">
                    <div class="index-row-1">
                        <div class="model-name"><p>Role</p></div>
                        <div class="btn">
                        <div><a href="role/create">Create</a></div>
                        </div>
                    </div>
                </div>
        
        <div class="index-column-1">
            @include('role.select',$roles)
        </div>        
    </div>
</main>
@endsection