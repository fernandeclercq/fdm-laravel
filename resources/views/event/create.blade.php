@extends('layouts.app')
@section('content')
    <header>
        <div class="back-btn">
            <a href="{{secure_asset('admin')}}">Website<br>Index</a>
        </div>
        <h1>Fric-frac</h1>
    </header>
    <main>
    <div class="container-index-model">
    <div class="index-column-0">
        <div class="index-row-1">
            <div class="model-name"><p>Event</p></div>
            <div class="btn">
                <div><p>Create</p></div>
                <div><a href="{{secure_asset('/admin/event')}}">Annuleren</a></div>
            </div>
        </div>
        <div class="index-row-2">
            <form action="{{secure_asset('admin/event/store')}}" method="POST">
                @csrf
                <div class="form-fields-event">
                    <label for="Name">Naam</label>
                    <input type="text" name="Name">
                </div>
                <div class="form-fields-event">        
                    <label for="Location">Location</label>
                    <input type="text" name="Location">
                </div>
                <div class="form-fields-event">        
                    <label for="Starts">Starts</label>
                    <input type="date" name="Starts">
                </div>
                <div class="form-fields-event">        
                    <label for="Ends">Ends</label>
                    <input type="date" name="Ends">
                </div>
                <div class="form-fields-event">        
                    <label for="Image">Image</label>
                    <input type="text" name="Image">
                </div>
                <div class="form-fields-event">
                    <label for="Description">Description</label>
                    <textarea name="Description"></textarea>
                </div>
                <div class="form-fields-event">        
                    <label for="OrganiserName">Organiser Name</label>
                    <input type="text" name="OrganiserName">
                </div>
                <div class="form-fields-event">        
                    <label for="OrganiserDescription">Organiser Description</label>
                    <input type="text" name="OrganiserDescription">
                </div>
                <div class="form-fields-event">
                    <label for="EventCategory">Event Category</label>
                    <select name="EventCategory">
                        @foreach ($eventcategories as $category)
                            <option value="{{$category['Id']}}">{{$category['Name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-fields-event">
                        <label for="EventTopic">Event Topic</label>
                        <select name="EventTopic">
                            @foreach ($eventtopics as $topic)
                                <option value="{{$topic['Id']}}">{{$topic['Name']}}</option>
                            @endforeach
                        </select>
                </div>

                <div class="submit-field">
                    <button type="submit" name="create">Create</button>
                </div>
            </form>
        </div>
    </div>
        
        <div class="index-column-1">
            @include('event.select',$events)

        </div>        
    </div>
</main>
@endsection