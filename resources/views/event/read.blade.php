@extends('layouts.app')
@section('content')
    <header>
        <div class="back-btn">
            <a href="{{secure_asset('admin')}}">Website<br>Index</a>
        </div>
        <h1>Fric-frac</h1>
    </header>
    <main>
    <div class="container-index-model">
    <div class="index-column-0">
        <div class="index-row-1">
            <div class="model-name"><p>Event</p></div>
            <div class="btn">
                <div><a href="{{secure_asset("/admin/event/edit/$event->Id")}}">Update</a></div>
                <div><a href="{{secure_asset('/admin/event/create')}}">Create</a></div>
                <div><a href="{{secure_asset("/admin/event/delete/$event->Id")}}">Delete</a></div>
                <div><a href="{{secure_asset('/admin/event')}}">Annuleren</a></div>
            </div>
        </div>
        <div class="index-row-2">
        <form action="" method="POST">
                @csrf
                <div class="details-info">
                    <h3>Details - {{$event->Name}}</h3>
                    <p>Select the next action please</p>
                </div>
                <div class="form-fields-event">
                    <label for="Name">Naam</label>
                <input type="text" name="Name" readonly value="{{$event->Name}}">
                </div>
                <div class="form-fields-event">        
                    <label for="Location">Location</label>
                <input type="text" name="Code" readonly value="{{$event->Location}}">
                </div>
                <div class="form-fields-event">        
                    <label for="Starts">Starts</label>
                    <input type="date" name="Starts" readonly value="{{$event->Starts}}">
                </div>
                <div class="form-fields-event">        
                    <label for="Ends">Ends</label>
                    <input type="date" name="Ends" readonly value="{{$event->Ends}}">
                </div>
                <div class="form-fields-event">        
                    <label for="Image">Image</label>
                    <input type="text" name="Image" readonly value="{{$event->Image}}">
                </div>
                <div class="form-fields-event">
                    <label for="Description">Description</label>
                    <textarea name="Description" readonly>{{$event->Description}}</textarea>
                </div>
                <div class="form-fields-event">        
                    <label for="OrganiserName">Organiser Name</label>
                    <input type="text" name="OrganiserName" readonly value="{{$event->OrganiserName}}">
                </div>
                <div class="form-fields-event">        
                    <label for="OrganiserDescription">Organiser Description</label>
                    <input type="text" name="OrganiserDescription" readonly value="{{$event->OrganiserDescription}}">
                </div>
                <div class="form-fields-event">
                    <label for="EventCategory">Event Category</label>
                    <input type="text" name="EventCategory" readonly value="{{$eventcategory->Name}}">
                </div>
                <div class="form-fields-event">
                        <label for="EventTopic">Event Topic</label>
                <input type="text" name="EventTopic" readonly value="{{$eventtopic->Name}}">
                </div>
            </form>
        </div>
    </div>
        
        <div class="index-column-1">
            @include('event.select',$events)

        </div>        
    </div>
</main>
@endsection



