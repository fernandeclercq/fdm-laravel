@extends('layouts.app')
@section('content')
    <header>
        <div class="back-btn">
            <a href="{{secure_asset('admin')}}">Website<br>Index</a>
        </div>
        <h1>Fric-frac</h1>
    </header>
    <main>
    <div class="container-index-model">
    <div class="index-column-0">
        <div class="index-row-1">
            <div class="model-name"><p>Event</p></div>
            <div class="btn">
                <div><p>Update</p></div>
                <div><a href="{{secure_asset('/admin/event')}}">Annuleren</a></div>
            </div>
        </div>
        <div class="index-row-2">
        <form action="{{secure_asset('admin/event/update')}}" method="POST">
                @csrf
                <div class="details-info">
                    <h3>Edit - {{$event->Name}}</h3>
                    <p>Introduce the new data and click on Update</p>
                </div>
                    <input type="hidden" name="Id" value="{{$event->Id}}">
                    <div class="form-fields-event">
                            <label for="Name">Naam</label>
                        <input type="text" name="Name" value="{{$event->Name}}">
                        </div>
                        <div class="form-fields-event">        
                            <label for="Location">Location</label>
                        <input type="text" name="Location" value="{{$event->Location}}">
                        </div>
                        <div class="form-fields-event">        
                            <label for="Starts">Starts</label>
                            <input type="date" name="Starts" value="{{$event->Starts}}">
                        </div>
                        <div class="form-fields-event">        
                            <label for="Ends">Ends</label>
                            <input type="date" name="Ends" value="{{$event->Ends}}">
                        </div>
                        <div class="form-fields-event">        
                            <label for="Image">Image</label>
                            <input type="text" name="Image" value="{{$event->Image}}">
                        </div>
                        <div class="form-fields-event">
                            <label for="Description">Description</label>
                            <textarea name="Description">{{$event->Description}}</textarea>
                        </div>
                        <div class="form-fields-event">        
                            <label for="OrganiserName">Organiser Name</label>
                            <input type="text" name="OrganiserName" value="{{$event->OrganiserName}}">
                        </div>
                        <div class="form-fields-event">        
                            <label for="OrganiserDescription">Organiser Description</label>
                            <input type="text" name="OrganiserDescription" value="{{$event->OrganiserDescription}}">
                        </div>
                        <div class="form-fields-event">
                            <label for="EventCategory">Event Category</label>
                            <select name="EventCategory">
                                    @foreach ($eventcategories as $category)
                                        @if ($category['Id'] == $event->EventCategoryId)
                                            <option selected value="{{$category['Id']}}">{{$category['Name']}}</option>
                                        @else
                                            <option value="{{$category['Id']}}">{{$category['Name']}}</option>
                                        @endif  
                                    @endforeach
                            </select>
                        </div>
                        <div class="form-fields-event">
                                <label for="EventTopic">Event Topic</label>
                                <select name="EventTopic">
                                        @foreach ($eventtopics as $topic)
                                        @if ($topic['Id'] == $event->EventTopicId)
                                            <option selected value="{{$topic['Id']}}">{{$topic['Name']}}</option>
                                        @else
                                            <option value="{{$topic['Id']}}">{{$topic['Name']}}</option>
                                        @endif 
                                        @endforeach
                                </select>
                        </div>

                        <div class="submit-field">
                                <button type="submit" name="update">Update</button>
                        </div>
            </form>
        </div>
    </div>
        
        <div class="index-column-1">
            @include('event.select',$events)

        </div>        
    </div>
</main>
@endsection



