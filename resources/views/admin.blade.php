@extends('layouts.app')
@section('content')
    <header><h1>Fric-frac</h1></header>
    <main>
    <div class="container-1">
        <div class="row-1">
            <div id="box-1">
                <a href="{{secure_asset('admin/person/')}}" class="tile"><span>Persoon</span></a>
            </div>
            <div id="box-2">
                <a href="{{secure_asset('admin/country/')}}"class="tile"><span>Land</span></a>
            </div>
            <div id="box-3">
                <a></a>
            </div>
        </div>
        <div class="row-2">
            <div id="box-4">
                <a></a>
            </div>
            <div id="box-6">
                <a href="{{secure_asset('admin/role/')}}"class="tile"><span>Rol</span></a>
            </div>
            <div id="box-6">
                <a href="{{secure_asset('admin/user/')}}" class="tile"><span>Gebruiker</span></a>
            </div>
            <div id="box-7">
                <a></a>
            </div>
        </div>
        <div class="row-3">
            <div id="box-8">
                <a href="{{secure_asset('admin/event/')}}" class="tile"><span>Event</span></a>
            </div>
            <div id="box-9">
                <a href="{{secure_asset('admin/eventcategory/')}}" class="tile"><span>Event Categorie</span></a> 
            </div>
            <div id="box-10">
                <a href="{{secure_asset('admin/eventtopic/')}}"class="tile"><span>Event Topic</span></a>
            </div>
            <div id="box-11">
                <a class="tile"></a>
            </div>
        </div>
    </div>
</main>
@endsection       
        