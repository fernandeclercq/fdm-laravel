@extends('layouts.app')
@section('content')
    <header>
        <div class="back-btn">
            <a href="{{secure_asset('admin')}}">Website<br>Index</a>
        </div>
        <h1>Fric-frac</h1>
    </header>
    <main>
    <div class="container-index-model">
    <div class="index-column-0">
        <div class="index-row-1">
            <div class="model-name"><p>Event Category</p></div>
            <div class="btn">
                <div><p>Update</p></div>
                <div><a href="{{secure_asset('/admin/eventcategory')}}">Annuleren</a></div>
            </div>
        </div>
        <div class="index-row-2">
        <form action="{{secure_asset('admin/eventcategory/update')}}" method="POST">
                @csrf
                <div class="details-info">
                    <h3>Edit - {{$eventcategory->Name}}</h3>
                    <p>Introduce the new data and click on Update</p>
                </div>
                    <input type="hidden" name="Id" readonly value="{{$eventcategory->Id}}">
                <div class="form-fields">
                    <label for="Name">Naam</label>
                <input type="text" name="Name" value="{{$eventcategory->Name}}">
                </div>
                <div>        
                </div>
                <div class="submit-field">
                    <button type="submit" name="update">Update</button>
                </div>
                
            </form>
        </div>
    </div>
        
        <div class="index-column-1">
            @include('eventcategory.select',$eventcategories)

        </div>        
    </div>
</main>
@endsection



