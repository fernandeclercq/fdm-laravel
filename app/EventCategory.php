<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCategory extends Model
{
    private $id;
    private $name;

    public function __get($name){
        return $this->$name;
    }

    public function __set($name,$value){
        return $this->$name = $value;
    }

    public static function setProperties($data){
        $result = new EventCategory;
        foreach($data as $key => $value){
            $result->$key = $value;
        }
        return $result;
    }
}
