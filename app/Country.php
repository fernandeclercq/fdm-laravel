<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    private $id;
    private $name;
    private $code;
    //private $date;

    public function __get($name){
        return $this->$name;
    }

    public function __set($name,$value){
        return $this->$name = $value;
    }

    public static function setProperties($data){
        $result = new Country;
        foreach($data as $key => $value){
            $result->$key = $value;
        }
        return $result;
    }
}
