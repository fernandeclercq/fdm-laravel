<?php

namespace App\Http\Controllers;

use App\EventTopic;
use DB;
use Illuminate\Http\Request;
use ReminderController;

class EventTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventtopics = $this->getList();
        return view('eventtopic.index',['eventtopics'=> $eventtopics]);
    }

    public static function getList(){
        $result = DB::select('SELECT * FROM EventTopic');
        return (EventTopic::hydrate($result));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $eventtopics = $this->getList();
        return view('eventtopic.create',['eventtopics'=> $eventtopics]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('EventTopic')->insert(
            ['Name' => "$request[Name]"]
        );
        return redirect()->action('EventTopicController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventTopic  $eventtopic
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $eventtopics = $this->getList();
        $result = DB::table('EventTopic')->where('Id',$id)->first();
        
        return view('eventtopic.read',['eventtopic' => EventTopic::setProperties($result),
                                    'eventtopics' => $eventtopics]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventTopic  $eventtopic
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventtopics = $this->getList();
        $result = DB::table('EventTopic')->where('Id',$id)->first();
        
        return view('eventtopic.edit',['eventtopic' => EventTopic::setProperties($result),
                                    'eventtopics' => $eventtopics]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventTopic  $eventtopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('EventTopic')
            ->where('Id', $request['Id'])
            ->update(['Name' => $request['Name']
                    ]);
        return redirect()->action('EventTopicController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventTopic  $eventtopic
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eventtopics = $this->getList();
        $result = DB::table('EventTopic')->where('Id',$id)->first();
        return view('eventtopic.delete',['eventtopic' => EventTopic::setProperties($result),
                                      'eventtopics' => $eventtopics]);
    }

    public function delete(Request $request){

        DB::table('EventTopic')->where('Id', $request['Id'])->delete();

        return redirect()->action('EventTopicController@index');
    }
}
