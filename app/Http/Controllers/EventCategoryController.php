<?php

namespace App\Http\Controllers;

use App\EventCategory;
use DB;
use Illuminate\Http\Request;
use ReminderController;

class EventCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventcategories = $this->getList();
        return view('eventcategory.index',['eventcategories'=> $eventcategories]);
    }

    public static function getList(){
        $result = DB::select('SELECT * FROM EventCategory');
        return (EventCategory::hydrate($result));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $eventcategories = $this->getList();
        return view('eventcategory.create',['eventcategories'=> $eventcategories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('EventCategory')->insert(
            ['Name' => "$request[Name]"]
        );
        return redirect()->action('EventCategoryController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventCategory  $eventcategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $eventcategories = $this->getList();
        $result = DB::table('EventCategory')->where('Id',$id)->first();
        
        return view('eventcategory.read',['eventcategory' => EventCategory::setProperties($result),
                                    'eventcategories' => $eventcategories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventCategory  $eventcategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventcategories = $this->getList();
        $result = DB::table('EventCategory')->where('Id',$id)->first();
        
        return view('eventcategory.edit',['eventcategory' => EventCategory::setProperties($result),
                                    'eventcategories' => $eventcategories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventCategory  $eventcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('EventCategory')
            ->where('Id', $request['Id'])
            ->update(['Name' => $request['Name']
                    ]);
        return redirect()->action('EventCategoryController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventCategory  $eventcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eventcategories = $this->getList();
        $result = DB::table('EventCategory')->where('Id',$id)->first();
        return view('eventcategory.delete',['eventcategory' => EventCategory::setProperties($result),
                                      'eventcategories' => $eventcategories]);
    }

    public function delete(Request $request){

        DB::table('EventCategory')->where('Id', $request['Id'])->delete();

        return redirect()->action('EventCategoryController@index');
    }
}
