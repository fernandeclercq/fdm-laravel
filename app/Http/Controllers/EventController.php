<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventCategory;
use App\EventTopic;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\EventCategoryController;
use App\Http\Controllers\EventTopicController;

class EventController extends Controller
{  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = $this->getList();
        
        return view('event.index',['events'=> $events]);
    }

    public static function getList(){
        $result = DB::select('SELECT * FROM `Event`');
        return (Event::hydrate($result));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $events = $this->getList();
        $eventcategories = EventCategoryController::getList();
        $eventtopics = EventTopicController::getList();
        return view('event.create',['events'=> $events,
                                    'eventcategories' => $eventcategories,
                                    'eventtopics' => $eventtopics]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('Event')->insert(
            ['Name' => "$request[Name]", 
             'Location' => "$request[Location]",
             'Starts' => "$request[Starts]",
             'Ends' => "$request[Ends]",
             'Image' => "$request[Image]",
             'Description' => "$request[Description]",
             'OrganiserName' => "$request[OrganiserName]",
             'OrganiserDescription' => "$request[OrganiserDescription]",
             'EventCategoryId' => "$request[EventCategory]",
             'EventTopicId' => "$request[EventTopic]"]
        );
        return redirect()->action('EventController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $events = $this->getList();
        $event = Event::setProperties(DB::table('Event')
                    ->where('Id',$id)
                    ->first());

        $eventcategory = EventCategory::setProperties(DB::table('EventCategory')
                            ->where('Id',$event->EventCategoryId)
                            ->first());

        $eventtopic = EventTopic::setProperties(DB::table('EventTopic')
                        ->where('Id',$event->EventTopicId)
                        ->first());

        return view('event.read',[
                        'event' => $event,
                        'events' => $events,
                        'eventcategory' => $eventcategory,
                        'eventtopic' => $eventtopic
                        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $events = $this->getList();
        $event = Event::setProperties(DB::table('Event')
                    ->where('Id',$id)
                    ->first());

        $eventcategory = EventCategory::setProperties(DB::table('EventCategory')
                            ->where('Id',$event->EventCategoryId)
                            ->first());
        $eventcategories = EventCategoryController::getList();

        $eventtopic = EventTopic::setProperties(DB::table('EventTopic')
                        ->where('Id',$event->EventTopicId)
                        ->first());
        $eventtopics = EventTopicController::getList();

        return view('event.edit',[
                        'event' => $event,
                        'events' => $events,
                        'eventcategory' => $eventcategory,
                        'eventcategories' => $eventcategories,
                        'eventtopic' => $eventtopic,
                        'eventtopics' => $eventtopics
                        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('Event')
            ->where('Id', $request['Id'])
            ->update([
                    'Name' => "$request[Name]", 
                    'Location' => "$request[Location]",
                    'Starts' => date('Y-m-d',\strtotime($request['Starts'])),
                    'Ends' => date('Y-m-d',\strtotime($request['Ends'])),
                    'Image' => "$request[Image]",
                    'Description' => "$request[Description]",
                    'OrganiserName' => "$request[OrganiserName]",
                    'OrganiserDescription' => "$request[OrganiserDescription]",
                    'EventCategoryId' => "$request[EventCategory]",
                    'EventTopicId' => "$request[EventTopic]"
                    ]);
        return redirect()->action('EventController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $events = $this->getList();
        $event = Event::setProperties(DB::table('Event')
                    ->where('Id',$id)
                    ->first());

        $eventcategory = EventCategory::setProperties(DB::table('EventCategory')
                            ->where('Id',$event->EventCategoryId)
                            ->first());

        $eventtopic = EventTopic::setProperties(DB::table('EventTopic')
                        ->where('Id',$event->EventTopicId)
                        ->first());

        return view('event.delete',[
                        'event' => $event,
                        'events' => $events,
                        'eventcategory' => $eventcategory,
                        'eventtopic' => $eventtopic
                        ]);
    }

    public function delete(Request $request){

        DB::table('Event')->where('Id', $request['Id'])->delete();

        return redirect()->action('EventController@index');
    }
}
