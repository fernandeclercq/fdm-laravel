<?php

namespace App\Http\Controllers;

use App\Role;
use DB;
use Illuminate\Http\Request;
use ReminderController;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->getList();
        return view('role.index',['roles'=> $roles]);
    }

    private function getList(){
        $result = DB::select('SELECT * FROM Role');
        return (Role::hydrate($result));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->getList();
        return view('role.create',['roles'=> $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('Role')->insert(
            ['Name' => "$request[Name]"]
        );
        return redirect()->action('RoleController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = $this->getList();
        $result = DB::table('Role')->where('Id',$id)->first();
        
        return view('role.read',['role' => Role::setProperties($result),
                                    'roles' => $roles]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = $this->getList();
        $result = DB::table('Role')->where('Id',$id)->first();
        
        return view('role.edit',['role' => Role::setProperties($result),
                                    'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('Role')
            ->where('Id', $request['Id'])
            ->update(['Name' => $request['Name']
                    ]);
        return redirect()->action('RoleController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roles = $this->getList();
        $result = DB::table('Role')->where('Id',$id)->first();
        return view('role.delete',['role' => Role::setProperties($result),
                                      'roles' => $roles]);
    }

    public function delete(Request $request){

        DB::table('Role')->where('Id', $request['Id'])->delete();

        return redirect()->action('RoleController@index');
    }
}
