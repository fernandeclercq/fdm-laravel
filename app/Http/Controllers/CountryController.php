<?php

namespace App\Http\Controllers;

use DB;
use App\Country;
use Illuminate\Http\Request;
use ReminderController;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = $this->getList();
        return view('country.index',['countries'=> $countries]);
    }

    private function getList(){
        $result = DB::select('SELECT * FROM Country');
        return (Country::hydrate($result));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $countries = $this->getList();
        return view('country.create',['countries'=> $countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('Country')->insert(
            ['Name' => "$request[Name]", 
             'Code' => "$request[Code]"]
        );
        return redirect()->action('CountryController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $countries = $this->getList();
        $result = DB::table('Country')->where('Id',$id)->first();
        
        return view('country.read',['country' => Country::setProperties($result),
                                    'countries' => $countries]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries = $this->getList();
        $result = DB::table('Country')->where('Id',$id)->first();
        
        return view('country.edit',['country' => Country::setProperties($result),
                                    'countries' => $countries]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('Country')
            ->where('Id', $request['Id'])
            ->update(['Name' => $request['Name'],
                      'Code' => $request['Code']
                    ]);
        return redirect()->action('CountryController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $countries = $this->getList();
        $result = DB::table('Country')->where('Id',$id)->first();
        return view('country.delete',['country' => Country::setProperties($result),
                                      'countries' => $countries]);
    }

    public function delete(Request $request){

        DB::table('Country')->where('Id', $request['Id'])->delete();

        return redirect()->action('CountryController@index');
    }
}
