<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Event extends Model
{
    private $id; // int
    private $name;
    private $location;
    private $starts; // date
    private $ends;  // date
    private $image;
    private $description;
    private $organiserName;
    private $organiserDescription;
    private $eventcategoryId; //int
    private $eventtopicId;      //int


    public function __get($name){
        return $this->$name;
    }

    public function __set($name,$value){
        return $this->$name = $value;
    }

    public static function setProperties($data){
        $result = new Event;
        foreach($data as $key => $value){
            $result->$key = $value;
        }
        return $result;
    }
}
